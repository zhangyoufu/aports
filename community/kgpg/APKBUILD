# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kgpg
pkgver=21.04.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, mips64 and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !mips64 !riscv64"
url="https://kde.org/applications/utilities/org.kde.kgpg"
pkgdesc="A simple interface for GnuPG, a powerful encryption utility"
license="GPL-2.0-or-later"
makedepends="
	akonadi-contacts-dev
	extra-cmake-modules
	gpgme-dev
	karchive-dev
	kcodecs-dev
	kcontacts-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kgpg-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# kgpg-import fails too often
	# kgpg-encrypt and kgpg-export are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "kgpg-(import|encrypt|export)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e2c26bc093cafdc41d77beacb161f1695d9d7326f0156f15862dbccc81492bb0976febc898a45bc41f9f438538ee24d517ad250851271740bb9e90c420359499  kgpg-21.04.2.tar.xz
"
